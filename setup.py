from setuptools import setup

setup(
    name='mastermind',
    version='1.0.0',
    description=
    'This module is responsible for comparing the collected data with stored',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/recipes-desire/python-iot/python3-mastermind',
    license='MIT license',
    packages=['mastermind', 'mastermind.general_rules'],
    #package_data=[],
    #entry_points={
    #    'console_scripts': [
    #        'messageqclient=messageqclient.__main__:main'
    #    ]
    #},
    #install_requires=install_requires,
    #extras_require={},
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Programming Language :: Python :: 3.4',
        'Topic :: AI :: evaluation :: classification',
    ],
    zip_safe=True)
