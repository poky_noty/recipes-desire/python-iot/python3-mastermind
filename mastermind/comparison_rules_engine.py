from .general_rules.rule_equals_datasets import RuleEquals
from .comparison_rules_names import ComparisonRulesNames

class ComparisonRulesEngine(object):
    def __init__(self, requested_rule):
        self.requested_rule = requested_rule

    def select_rule_engine(self):
        if self.requested_rule == ComparisonRulesNames.EQUAL:
            # Return class
            return RuleEquals
        else:
            raise NotImplementedError("Unknown Rule Engine !")

    def dummy(self):
        pass
