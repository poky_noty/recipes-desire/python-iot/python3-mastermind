from .net_generic_data import NetGenericData


class PortScanningData(NetGenericData):
    def __init__(self):
        pass

    def cross_validation_new_stored(self):
        NetGenericData.cross_validation_new_stored(self)

    def dummy(self):
        pass
