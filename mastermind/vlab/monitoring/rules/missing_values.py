from .generic_rule import GenericRule

class MissingValuesRule(GenericRule):
    def __init__(self, stored_data, collected_data):
        GenericRule.__init__(self, stored_data, collected_data)
   
    def apply_rule(self):
        GenericRule.apply_rule(self)
