
from .net_generic_data import NetGenericData


class HostDiscoveryData(NetGenericData):
    def __init__(self, new_book):
        NetGenericData.__init__(self, new_book)

    def handle_new_hosts_up_list(self):
        pass

    def cross_validation_new_stored(self):
        NetGenericData.cross_validation_new_stored(self)
