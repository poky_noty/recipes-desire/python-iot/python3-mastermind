on target:

pydeps ./usr/lib/python3.7/site-packages/mastermind --show-dot --noshow --pylib

on host:

copy over the result to host as mastermind.dot

dot -Tsvg mastermind.dot -o mastermind.svg
